# Setup this dotfile on a new computer


~~~{.bash}
git clone --bare https://gitlab.com/Syllo/dotfiles.git $HOME/Repository/dotfiles
alias dotfiles='git --git-dir=$HOME/Repository/dotfiles/ --work-tree=$HOME'
dotfiles checkout
dotfiles config --local status.showUntrackedFiles no
~~~
