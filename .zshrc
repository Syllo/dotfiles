HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd beep nomatch appendhistory
unsetopt notify
bindkey -v

# Completion
autoload -Uz compinit
compinit
# Use default and approximate completers
zstyle ':completion:*' completer _extensions _complete _approximate
# Automatically highlight first element of completion menu
setopt MENU_COMPLETE
# Automatically list choices on ambiguous completion.
setopt AUTO_LIST
# Complete from both ends of a word.
setopt COMPLETE_IN_WORD
# Enable completion menu
zstyle ':completion:*' menu select
# Autocomplete options
zstyle ':completion:*' complete-options true
# Matching files by modification date (most recently modified first)
zstyle ':completion:*' file-sort modification
# Use a cache to speedup completion
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ".cache/zsh/.zcompcache"
# Improve completion menu sections display
zstyle ':completion:*:*:*:*:corrections' format '%F{yellow}!- %d (errors: %e) -!%f'
zstyle ':completion:*:*:*:*:descriptions' format '%F{blue}-- %D %d --%f'
zstyle ':completion:*:*:*:*:messages' format ' %F{purple} -- %d --%f'
zstyle ':completion:*:*:*:*:warnings' format ' %F{red}-- no matches found --%f'
# Use the same colors as the ls command for files/directories etc
zstyle ':completion:*:*:*:*:default' list-colors ${(s.:.)LS_COLORS}

PATH="$HOME/.local/bin:$HOME/.cargo/bin:$PATH"

[[ $- = *i* ]] || return # skip if non interactive

alias gcc='gcc -fdiagnostics-color=always'
alias grep='grep --color=auto'
alias ll='ls -l --color=auto'
alias la='ls -A --color=auto'
alias ls='ls --color=auto'
alias dir='dir --color=auto'
alias vim=$(command -v nvim || command -v vim || echo vim)
alias dotfiles='git --git-dir=$HOME/Repository/dotfiles/ --work-tree=$HOME'
if command -v bat 2>/dev/null 1>&2; then alias cat='bat'; fi
if command -v prettyping 2>/dev/null 1>&2; then alias ping='prettyping'; fi
if command -v ncdu 2>/dev/null 1>&2; then alias du='ncdu --color dark -r -rr'; fi
if command -v rsync 2>/dev/null 1>&2; then alias scp='rsync --info=progress2 --partial --no-inc-recursive'; fi

#Less colors for man
export LESS_TERMCAP_mb=$'\E[05;31m'               # begin blinking
export LESS_TERMCAP_md=$'\E[01;38;5;74m'          # begin bold
export LESS_TERMCAP_me=$'\E[0m'                   # end blink/bold/underline
export LESS_TERMCAP_so=$'\E[38;5;016;48;5;62m'    # begin standout-mode - info box
export LESS_TERMCAP_se=$'\E[0m'                   # end standout-mode
export LESS_TERMCAP_us=$'\E[04;38;5;146m'         # begin underline
export LESS_TERMCAP_ue=$'\E[0m'                   # end underline
export GROFF_NO_SGR=1

# Disable vcs info in sshfs mounts
zstyle ':vcs_info:*' disable-patterns "${(b)HOME}/sshfs(|/*)"

# Liquid Prompt configuration
export LP_ENABLE_TEMP=0
export LP_ENABLE_TIME=1

if [ -f ~/.local/share/dotfiles/antigen/antigen.zsh ]
then
  source ~/.local/share/dotfiles/antigen/antigen.zsh

  antigen init ~/.antigenrc
fi

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
