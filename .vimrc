" Set shell as bash
set shell=/bin/bash

"Set utf-8 encoding
if !has('nvim')
  set encoding=utf-8
endif

" When started as "evim", evim.vim will already have done these settings.
if v:progname =~? "evim"
    finish
endif

" Use Vim settings, rather than Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

"Set terminal 256 colors
set t_Co=256
set t_ut=

" allow backspacing over everything in insert mode
set backspace=indent,eol,start

set history=50  " keep 50 lines of command line history
set ruler       " show the cursor position all the time
set showcmd     " display incomplete commands
set incsearch   " do incremental searching
set wildmenu    "command-line completion show a list of matches


" Don't use Ex mode, use Q for formatting
map Q gq

" CTRL-U in insert mode deletes a lot.  Use CTRL-G u to first break undo,
" so that you can undo CTRL-U after inserting a line break.
inoremap <C-U> <C-G>u<C-U>

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
    set mouse=a
endif

" Only do this part when compiled with support for autocommands.
if has("autocmd")

    " Enable file type detection.
    " Use the default filetype settings, so that mail gets 'tw' set to 72,
    " 'cindent' is on in C files, etc.
    " Also load indent files, to automatically do language-dependent indenting.
    filetype plugin indent on

    " Put these in an autocmd group, so that we can delete them easily.
    augroup vimrcEx
        au!

        " When editing a file, always jump to the last known cursor position.
        " Don't do it when the position is invalid or when inside an event handler
        " (happens when dropping a file on gvim).
        " Also don't do it when the mark is in the first line, that is the default
        " position when opening a file.
        autocmd BufReadPost *
                    \ if line("'\"") > 1 && line("'\"") <= line("$") |
                    \   exe "normal! g`\"" |
                    \ endif

    augroup END

else

    set autoindent        " always set autoindenting on

endif " has("autocmd")

" Switch syntax highlighting on, when the terminal has colors
" Also switch on highlighting the last used search pattern.
if &t_Co > 2 || has("gui_running")
    syntax on
    set hlsearch
endif

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
    command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
                \ | wincmd p | diffthis
endif

"" Text searching and patterns

"Set the ignore case option
set ignorecase
"If pattern contains uppercase ignore ignore-case
set smartcase

"" Text display

"Set the tabulation view as >- and space as -
set list
set listchars=tab:⇥·,trail:·

"Set the wrap on long lines
set wrap

"Set two lines around the cursor and the border
set scrolloff=2

if has("linebreak")
    "break line at specific characters
    set linebreak

    "set the character to indicate a wrapped line
    set showbreak=↯\ 
endif

" Hiding buffer instead of closing them
set hidden

"Set the line number
set number

"limit the text to 150 characters
set textwidth=150

if has("syntax")
    "" Syntax, highlighting and spelling

    "Color the 80th column
    set colorcolumn=80

    "Set the column highlighting if in insert mode
    autocmd InsertLeave * setlocal nocursorcolumn
    autocmd InsertLeave * setlocal nocursorline
    autocmd InsertEnter * setlocal cursorcolumn
    autocmd InsertEnter * setlocal cursorline

    "Set the spell languages
    set spelllang=en
endif

"" Windows options

" Do not redraw when executing macros
set lazyredraw

let g:clipboard = {
      \   'name': 'myClipboard',
      \   'copy': {
      \      '+': 'tmux load-buffer -',
      \      '*': 'xclip -selection clipboard',
      \    },
      \   'paste': {
      \      '+': 'tmux save-buffer -',
      \      '*': 'xclip -o',
      \   },
      \   'cache_enabled': 1,
      \ }

"Always show the status bar
set laststatus=2

"Don't unload a buffer when no longer shown
set hidden

"" Text editing

"Show a [+] if file is modified
set modified

"" Tabs and indenting

"Set a tab to 4 spaces
set tabstop=2
set shiftwidth=2
set softtabstop=2

"Tabs are spaces
set smarttab
set expandtab

"Indentation
set autoindent

if has("smartindent")
    set smartindent
endif

"" Folding
if has("folding")
    "Open a fold every times we need it
    set foldopen=all
    "Close a fold when the cursor leaves it
    set foldclose=all
    "Set the folding method to the syntax meaning the language syntax create the syntax status
    set foldmethod=syntax
endif

"Change location of swap files
if ! isdirectory(expand("$HOME/.vim/swp")) && exists("*mkdir")
    call mkdir(expand("$HOME/.vim/swp"), "p")
endif
set directory=$HOME/.vim/swp

if has("persistent_undo")
    "Save undo files (after closing the files the undo is not lost)
    if ! isdirectory(expand("$HOME/.vim/undo")) && exists("*mkdir")
        call mkdir(expand("$HOME/.vim/undo"), "p")
    endif
    set undofile
    set undodir=$HOME/.vim/undo
endif

""Editing

if has("virtualedit")
    "Set virtualedit to block to allow to go below a line end in visual block
    set virtualedit=block
endif

" Key mapping
if filereadable(expand("$HOME/.vim/config/mapping.vim"))
  source ~/.vim/config/mapping.vim
endif

" Plugins
if filereadable(expand("$HOME/.vim/autoload/plug.vim")) && filereadable(expand("$HOME/.vim/vimPlug/plugConfig.vim"))
  source ~/.vim/vimPlug/plugConfig.vim
endif

"" Plugin options
if filereadable(expand("~/.vim/config/plugin_config.vim"))
    source ~/.vim/config/plugin_config.vim
endif

"" Syntax
if filereadable(expand("~/.vim/syntax_group.vim"))
    source ~/.vim/syntax_group.vim
endif

" ManPage in vim
runtime ftplugin/man.vim

autocmd BufRead,BufNewFile *.h,*.c set filetype=c
