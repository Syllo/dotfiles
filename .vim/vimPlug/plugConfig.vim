call plug#begin('~/.vim/activePluging')

" Control P
Plug 'ctrlpvim/ctrlp.vim'

" Buffer explorer
Plug 'fholgado/minibufexpl.vim'

" Color theme
Plug 'nanotech/jellybeans.vim'

" Neomake
Plug 'neomake/neomake'

" UndoTree
Plug 'mbbill/undotree'

" MIPS syntax highlighting
Plug '~/.vim/customPlugins/mips'

" Comments
Plug 'scrooloose/nerdcommenter'

" Auto close delimiter
Plug 'Raimondi/delimitMate'

" status bar
Plug 'vim-airline/vim-airline'

" Git sign column
Plug 'airblade/vim-gitgutter'

" Easy align
Plug 'junegunn/vim-easy-align'

" Y stack
Plug 'maxbrunsfeld/vim-yankstack'

" NerdTree
Plug 'scrooloose/nerdtree'

" LanguageTool
Plug 'dpelle/vim-LanguageTool'

" Tag bar
Plug 'majutsushi/tagbar'

" Close buffer without closing window
Plug 'moll/vim-bbye'

" Grammalecte support
Plug 'dpelle/vim-Grammalecte'

" Deoplete
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

Plug 'lervag/vimtex'

" Language Server Protocol
Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

" Fuzzy finding
Plug 'junegunn/fzf', { 'do': './install --all' }
Plug 'junegunn/fzf.vim'

Plug 'liuchengxu/vista.vim'

Plug 'jsfaint/gen_tags.vim'

Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() } }

call plug#end()
