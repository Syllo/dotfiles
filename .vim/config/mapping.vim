" Mapping for vim
"
" author: Maxime Schmitt

" Leader key
let g:mapleader=","
set tm=2000

" Press jk to escape insert mode
inoremap jk <esc>

"To change to the window on the top
map <C-k> <C-w>k
"To change to the window to the bottom
map <C-j> <C-w>j
"To change to the window to the left
map <C-h> <C-w>h
"To change to the window to the right
map <C-l> <C-w>l
nnoremap <space> zi

nmap <leader>xy "*y
nmap <leader>ty "+y
vmap <leader>xy "*y
vmap <leader>ty "+y
nmap <leader>xp "*p
nmap <leader>tp "+p
vmap <leader>xp "*p
vmap <leader>tp "+p
nmap <leader>b :Neomake!<CR>

vnoremap <silent> * :call VisualSelection('f', '')<CR>
vnoremap <silent> # :call VisualSelection('b', '')<CR>

if has('nvim')
  tnoremap <ESC> <C-\><C-n>
endif
