" Jellybeans colorscheme
let g:jellybeans_use_term_background_color=1
colorscheme jellybeans

" NerdTree
let g:NERDTreeWinSize = 35
let g:NERDTreeShowHidden=1
let g:NERDTreeIgnore=['\.swp$', '\~$']
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" Airline
set laststatus=2

" MiniBufExplorer
let g:miniBufExplVSplit = 20
let g:miniBufExplMaxSize = 20
let g:miniBufExplBRSplit = 0

" DelimitMate
let g:delimitMate_expand_cr = 1
let g:delimitMate_expand_space = 1

"" Key mapping

" MiniBufExplorer
nnoremap <silent> <F9> :MBEToggle<CR>

" Toogle NerdTree
nnoremap <leader>n :NERDTreeToggle<cr>

" EasyAlign
xmap <leader>a <Plug>(EasyAlign)

" YankStack
let g:yankstack_map_keys = 0
nmap <leader>p <Plug>yankstack_substitute_older_paste
nmap <leader>P <Plug>yankstack_substitute_newer_paste

set tags=./.ctags;

" CtrlP
let g:ctrlp_extensions = [ 'tag' , 'mixed' ]
let g:ctrlp_cmd = 'CtrlP'

" LanguageTool
let g:languagetool_jar = "~/LanguageTool/languagetool-commandline.jar"

" Tagbar
map <leader>tt :Tagbar<CR>

let g:grammalecte_cli_py="/usr/bin/grammalecte-cli"

" Deoplete
let g:deoplete#enable_at_startup = 1
inoremap <silent><expr> <TAB>
  \ pumvisible() ? "\<C-n>" :
  \ <SID>check_back_space() ? "\<TAB>" :
  \ deoplete#mappings#manual_complete()
function! s:check_back_space() abort "{{{
let col = col('.') - 1
return !col || getline('.')[col - 1]  =~ '\s'
endfunction"}}}

" Format C/C++
if(executable("clang-format"))
  autocmd FileType c,cpp,h,hpp setlocal equalprg=clang-format
endif

" Neomake
call neomake#configure#automake('w')
let g:neomake_clangcheck_args = [ '-p', 'build' ]
let g:neomake_clangtidy_args = [ '-p', 'build' ]
let g:neomake_c_enabled_makers = [ 'clangtidy', 'clangcheck', 'checkpatch', 'cppcheck' ]
let g:neomake_cpp_enabled_makers = [ 'clangtidy', 'clangcheck', 'checkpatch', 'cppcheck' ]

" Language Server Protocol
set hidden

let g:LanguageClient_serverCommands = {
    \ 'c': ['clangd', '-background-index', '-compile-commands-dir=build/', '--header-insertion=never'],
    \ 'cpp': ['clangd', '-background-index', '-compile-commands-dir=build/', '--header-insertion=never'],
    \ }
nnoremap <F5> :call LanguageClient_contextMenu()<CR>

let $FZF_DEFAULT_OPTS = '--layout=reverse'

let g:fzf_layout = { 'window': 'call OpenFloatingWin()' }

function! OpenFloatingWin()
  let height = &lines - 3
  let width = float2nr(&columns - (&columns * 1 / 10))
  let col = float2nr((&columns - width) / 2)

  let opts = {
        \ 'relative': 'editor',
        \ 'row': height * 0.3,
        \ 'col': col + 30,
        \ 'width': width,
        \ 'height': height * 2 / 3
        \ }

  let buf = nvim_create_buf(v:false, v:true)
  let win = nvim_open_win(buf, v:true, opts)

  call setwinvar(win, '&winhl', 'Normal:Pmenu')

  setlocal
        \ buftype=nofile
        \ nobuflisted
        \ bufhidden=hide
        \ nonumber
        \ norelativenumber
        \ signcolumn=no
endfunction

nnoremap <leader>] :call fzf#vim#tags('^' . expand('<cword>'), {'options': '--exact --select-1 --exit-0 +i'})<CR>

" VimTex
let g:tex_flavor = 'latex'
